<?php

if (isset($_POST['reset-submit'])) {

    $selector = bin2hex(random_bytes(8));
    $token = random_bytes(32);

    $url = "localhost/create-new-password.php?selector=".$selector."&validator=".bin2hex($token);

    $expires = date("U") + 1800;

    require 'dbh.inc.php';

    $email = $_POST['email'];

    $sql = "DELETE FROM resetPassword WHERE resetEmail=?";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../reset-password.php?sqlerror");
        exit();
    }
    else {
        mysqli_bind_param($stmt, "s", $email);
        mysqli_stmt_execute($stmt);
    }  

    $sql = "INSERT INTO resetPassword(resetEmail, resetSelector, resetToken, resetExpires) VALUE (?, ?, ?, ?)";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../reset-password.php?sqlerror");
        exit();
    }
    else {
        $tokenHash = password_hash($token, PASSWORD_DEFAULT);
        mysqli_bind_param($stmt, "ssss", $email, $selector, $tokenHash, $expires);
        mysqli_stmt_execute($stmt);
    }

    mysqli_stmt_close($stmt);
    mysqli_close($conn);

    $to = $email;
    $subject = 'Reset your password';
    $message = '<p>We received a request to reset your password. The link to reset your password is below. If you did not make this request, you can ignore this email.</p>';
    $message .= '<p>Here is your password reset link: </br>';
    $message .= '<a href="'.$url.'">'.$url.'</a></p>';

    $headers = "From: rkfd <rockfordapps@gmail.com>\r\n";
    $headers .= "Reply-To: rockfordapps@gmail.com\r\n";
    $headers .= "Content-type: text/html\r\n";

    mail($to, $subject, $message, $headers);

    header("Location: ../reset-password.php?reset=success");

}
else {
    header("Location: ../login.php");
    exit();
}

?>