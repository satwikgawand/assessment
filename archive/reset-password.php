<?php

session_start();

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="description" content="JurisTech Assessment for User Authentication with PHP and MySQL.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="wrapper">
        <h1 class="title">JurisTech Assessment</h1>
        <div class="form-container">
            <h1 class="form-title">Reset your password</h1>
            <p class="text">An email will be sent to you with instructions to reset your password</p>

            <?php
      
                if (isset($_GET['reset'])) {
                    if ($_GET['reset'] == "success") {
                        echo '<p class="text-success">Check your email!</p>';
                    }
                }
      
            ?>

            <form class="login-form" action="includes/reset-password.inc.php" method="post">
                <input class="form-field" type="text" name="email" placeholder="your email"><br />
                <button class="form-submit" type="submit" name="reset-submit">Send</button>
            </form>
            <div class="login-signup">
                <p class="text">don't have an account with us yet? <a class="toggle" href="signup.php">Signup</a>.</p>
                <p class="text">or go back to the <a class="toggle" href="index.php">main page</a>.</p>
            </div>
        </div>
    </div>
</body>

</html>