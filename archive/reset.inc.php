<?php

    if (isset($_POST['reset-submit'])) {

        $selector = $_POST['selector'];
        $validator = $_POST['validator'];
        $password = $_POST['pwd'];
        $passwordRepeat = $_POST['pwd-repeat'];

        if (empty($password) || empty($passwordRepeat)) {
            header("Location: ../create-new-password.php?error=emptyfields");
            exit();
        }
        else if ($password != $passwordRepeat) {
            header("Location: ../create-new-password.php?error=mismatch");
            exit();
        }

        $currDate = date("U");
        
        require 'dbh.inc.php';

        $sql = "SELECT * FROM resetPassword WHERE resetSelector=? AND resetExpires>=?";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../create-new-password.php?sqlerror");
            exit();
        }
        else {
            mysqli_bind_param($stmt, "ss", $selector, $currDate);
            mysqli_stmt_execute($stmt);

            $result = mysqli_stmt_get_result($stmt);

            if (!$row = mysqli_fetch_assoc($result)) {
                echo 'You need to re-submit your reset request!';
                exit();
            }
            else {
                $tokenBin = hex2bin($validator);
                $tokenCheck = password_verify($tokenBin, $row['resetToken']);

                if ($tokenCheck == false) {
                    echo 'You need to re-submit your reset request!';
                    exit();
                }
                else if ($tokenCheck == true) {
                    $tokenEmail = $row['resetEmail'];

                    $sql = "SELECT * FROM users WHERE emailUsers=?";
                    $stmt = mysqli_stmt_init($conn);

                    if (!mysqli_stmt_prepare($stmt, $sql)) {
                        header("Location: ../create-new-password.php?sqlerror");
                        exit();
                    }
                    else {
                        mysqli_stmt_bind_param($stmt, "s", $tokenEmail);
                        mysqli_stmt_execute($stmt);

                        $result = mysqli_stmt_get_result($stmt);

                        if (!$row = mysqli_fetch_assoc($result)) {
                            header("Location: ../create-new-password.php?sqlerror");
                            exit();
                        }
                        else {

                            $sql = "UPDATE users SET pwdUsers=? WHERE emailUsers=?";
                            $stmt = mysqli_stmt_init($conn);

                            if (!mysqli_stmt_prepare($stmt, $sql)) {
                                header("Location: ../create-new-password.php?sqlerror");
                                exit();
                            }
                            else {

                                $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                                mysqli_stmt_bind_param($stmt, "ss", $passwordHash, $tokenEmail);
                                mysqli_stmt_execute($stmt);

                                $sql = "DELETE FROM resetPassword WHERE resetEmail=?";
                                $stmt = mysqli_stmt_init($conn);

                                if (!mysqli_stmt_prepare($stmt, $sql)) {
                                    header("Location: ../create-new-password.php?sqlerror");
                                    exit();
                                }
                                else {
                                    mysqli_stmt_bind_param($stmt, "s", $tokenEmail);
                                    mysqli_stmt_execute($stmt);
                                    header("Location: ../login.php?reset=updated");
                                }

                            }

                        }
                    
                    }

                }
            
            }
        
        }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);

    }
    else {
        header("Location: ../index.php");
        exit();
    }

?>