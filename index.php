<?php

session_start();

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="description" content="JurisTech Assessment for User Authentication with PHP and MySQL.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="wrapper">
        <h1 class="title">JurisTech Assessment</h1>
        <div class="container">

            <p class="text"> This is the Developed Solution for JurisTech Assessment for user authentication with PHP and MySQL.</p>
            <br /><br />
            <?php

                if (isset($_SESSION['userId'])) {
                    $usrnm = $_SESSION['userUsername'];
                    echo '
                        <p class="text">Welcome, '.$usrnm.'. You are logged in.</p>
                        <form action="includes/logout.inc.php" method="post">
                            <button class="form-submit" type="submit" name="logout-submit">Logout</button>
                        </form>
                    ';
                }
                else {
                    echo '
                        <p class="text">Please Login or Signup to continue.</p>
                        <p class="text">To Login, please <a class="toggle" href="login.php">click here</a>.</p>
                        <p class="text">To Signup, please <a class="toggle" href="signup.php">click here</a>.</p>
                    ';
                }

            ?>

        </div>

    </div>
</body>

</html>