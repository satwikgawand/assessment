-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 06, 2019 at 10:56 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assessment`
--

-- --------------------------------------------------------

--
-- Table structure for table `histPassword`
--

CREATE TABLE `histPassword` (
  `histId` int(11) NOT NULL,
  `histEmail` text NOT NULL,
  `histPass` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `histPassword`
--

INSERT INTO `histPassword` (`histId`, `histEmail`, `histPass`) VALUES
(7, 'qq@qq.com', '$2y$10$HlpEatVyiLAjdD3qRsxayuQj6.kseT5TVJwLiWQUoO8U7dHSOuIe.'),
(9, 'qq@qq.com', '$2y$10$5ZVGnK/X8WSohzqwgf2RBuOFURhfO01ZrQ6bz1xuQBeYx5XyuCZRe'),
(10, 'qq@qq.com', '$2y$10$Uvrc8tZD7meXQTMPVglq5ek9qmZSC/SgzElXbMOmtW7XU8COLO8xq'),
(11, 'qq@qq.com', '$2y$10$/oQPuEvCAXB93jpb5uxVM.fq2CRAKS4aoLQCD5jiksoqycSCttJXW'),
(12, 'qq@qq.com', '$2y$10$Shl9iFjKanwHtJ2Zv1rF9O62hxvqtkvhByhP80hK0bg5tz6pvucuy'),
(13, 'qq@qq.com', '$2y$10$EdOugOQ8kaBUkxYLGNw.duKrI83KVgJf8VpJXlc3Eaef0T9wgb3wW'),
(14, 'pp@pp.pp', '$2y$10$bPK8O/BzMSMej5eS84ekhOK2FlNRaq7GKFY/6qiI4LIJVveB6oqSC'),
(15, 'my@email.my', '$2y$10$95GWkXnN8C/mOLgbAlq3cuEmZ8vtT64n9X2q5k1f2VGJFwSb9Zu7S'),
(16, 'my@email.my', '$2y$10$sT0p40LcIjuXKd1omv9CAu4heG.kpto9f0AuuA0fOBX4qwZ1x0Xz2'),
(17, 'my@email.my', '$2y$10$rjqhsFKlmZMeLCmorCFA2usZKR3pWHXC5WWCT5VXn0K90bZ.wxTsy');

-- --------------------------------------------------------

--
-- Table structure for table `resetPassword`
--

CREATE TABLE `resetPassword` (
  `resetId` int(11) NOT NULL,
  `resetEmail` text NOT NULL,
  `resetSelector` text NOT NULL,
  `resetToken` longtext NOT NULL,
  `resetExpires` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUsers` int(11) NOT NULL,
  `usernameUsers` tinytext NOT NULL,
  `emailUsers` tinytext NOT NULL,
  `pwdUsers` longtext NOT NULL,
  `questionUsers` text NOT NULL,
  `answerUsers` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`idUsers`, `usernameUsers`, `emailUsers`, `pwdUsers`, `questionUsers`, `answerUsers`) VALUES
(8, 'qq', 'qq@qq.com', '$2y$10$EdOugOQ8kaBUkxYLGNw.duKrI83KVgJf8VpJXlc3Eaef0T9wgb3wW', 'first-kiss', '$2y$10$KyLy.jYLK9FnQsYYvGW6vOwoG4nJMiD5nz7b2issSd818HHfoIa3a'),
(9, 'pp', 'pp@pp.pp', '$2y$10$bPK8O/BzMSMej5eS84ekhOK2FlNRaq7GKFY/6qiI4LIJVveB6oqSC', 'first-job-place', '$2y$10$1D4TO.MCvrzC3ICuIytGveqZVXyci9ttYJ3mP7/AKcA9nHUB954uK'),
(10, 'myusername', 'my@email.my', '$2y$10$rjqhsFKlmZMeLCmorCFA2usZKR3pWHXC5WWCT5VXn0K90bZ.wxTsy', 'first-job-place', '$2y$10$5vkXv207EvQ2Wv9k9ywu6.z3UUubFcbdkBJUFXmdf03K5CX5.Jli.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `histPassword`
--
ALTER TABLE `histPassword`
  ADD PRIMARY KEY (`histId`);

--
-- Indexes for table `resetPassword`
--
ALTER TABLE `resetPassword`
  ADD PRIMARY KEY (`resetId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`idUsers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `histPassword`
--
ALTER TABLE `histPassword`
  MODIFY `histId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `resetPassword`
--
ALTER TABLE `resetPassword`
  MODIFY `resetId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUsers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
