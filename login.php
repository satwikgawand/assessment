<?php

session_start();

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="description" content="JurisTech Assessment for User Authentication with PHP and MySQL.">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
</head>

<body>
  <div class="wrapper">
    <h1 class="title">JurisTech Assessment</h1>
    <div class="container">
      <h1 class="form-title">Login</h1>

      <?php

        if (isset($_GET['error'])) {

          if ($_GET['error'] == "emptyfields") {
            echo '<p class="text-error">Fill in all fields!</p>';
          }
          else if ($_GET['error'] == "wrongpassword") {
            echo '<p class="text-error">Incorrect password!</p>';
          }
          else if ($_GET['error'] == "nouser") {
            echo '<p class="text-error">No user with this username or email is registered! Please Signup <a class="toggle" href="signup.php">here</a></p>';
          }

        }

      ?>

      <form class="login-form" action="includes/login.inc.php" method="post">
        <input class="form-field" type="text" name="idmail" placeholder="username or email"><br />
        <input class="form-field" type="password" name="pwd" placeholder="password"><br />
        <button class="form-submit" type="submit" name="login-submit">Login</button>
      </form>
      <div class="login-signup">
        <a class="toggle" href="forgot-password.php">Forgot your password?</a>
        <p class="text">don't have an account with us yet? <a class="toggle" href="signup.php">Signup</a>.</p>
        <p class="text">or go back to the <a class="toggle" href="index.php">main page</a>.</p>
      </div>
    </div>
  </div>
</body>

</html>