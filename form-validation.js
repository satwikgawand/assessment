var invalid = '#fc8f85';
var valid = '#ffffff';

// username
function usernameValidation() {
    let username = document.getElementById('username').value;
    if (username.trim() == "" || username.length < 5) {
        document.getElementById('username').style.backgroundColor = invalid;
        document.getElementById('username-helper').style.display = 'block';
    }
    else {
        document.getElementById('username').style.backgroundColor = valid;
        document.getElementById('username-helper').style.display = 'none';
    }
}

//email
function emailValidation() {
    let email = document.getElementById('email').value;
    let emailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailformat.test(email)) {
        document.getElementById('email').style.backgroundColor = invalid;
        document.getElementById('email-helper').style.display = 'block';
    }
    else {
        document.getElementById('email').style.backgroundColor = valid;
        document.getElementById('email-helper').style.display = 'none';
    }
}

// password
function passwordValidation() {
    let password = document.getElementById('password').value;
    if(password.trim() == "" || password.length < 8) {
        document.getElementById('password').style.backgroundColor = invalid;
        document.getElementById('password-helper').style.display = 'block';
    }
    else {
        document.getElementById('password').style.backgroundColor = valid;
        document.getElementById('password-helper').style.display = 'none';
    }
}

function repeatPasswordValidation() {
    let password = document.getElementById('password').value;
    let repeatPassword = document.getElementById('repeat-password').value;
    if(password != repeatPassword) {
        document.getElementById('repeat-password').style.backgroundColor = invalid;
        document.getElementById('repeat-password-helper').style.display = 'block';
    }
    else {
        document.getElementById('repeat-password').style.backgroundColor = valid;
        document.getElementById('repeat-password-helper').style.display = 'none';
    }
}