<?php

session_start();

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="description" content="JurisTech Assessment for User Authentication with PHP and MySQL.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="wrapper">
        <h1 class="title">JurisTech Assessment</h1>
        <div class="container">
            <?php
            if (isset($_SESSION['userUsername'])) {
                $usrnm = $_SESSION['userUsername'];
                echo '<p>Hello'.$usrnm.'</p>';
            }
            else {
                echo '<p>No Session</p>';
            }
            ?>
            <p class="text">Logged in!</p>

        </div>

    </div>
</body>

</html>