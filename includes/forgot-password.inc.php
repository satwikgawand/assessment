<?php

    if (isset($_POST['forgot-submit'])) {

        require 'dbh.inc.php';

        $question = $_POST['security-question'];
        $answer = $_POST['security-answer'];
        $email = $_POST['forgot-email'];
        $newpass = $_POST['new-pass'];
        $repeatNewpass = $_POST['repeat-new-pass'];

        if (empty($answer) || empty($email) || empty($newpass) || empty($repeatNewpass)) {
            header("Location: ../forgot-password.php?error=emptyfields");
            exit();
        }
        else if ($question == "select") {
            header("Location: ../forgot-password.php?error=noques");
            exit();
        }
        else if ($newpass != $repeatNewpass) {
            header("Location: ../forgot-password.php?error=mismatchpass");
            exit();
        }
        else {

            $sql = "SELECT * FROM users WHERE emailUsers=?";
            $stmt = mysqli_stmt_init($conn);

            if (!mysqli_stmt_prepare($stmt,$sql)) {
                header("Location: ../forgot-password.php?error=sqlerror");
                exit();
            }
            else {

                $answerHash = password_hash($answer, PASSWORD_DEFAULT);
                mysqli_stmt_bind_param($stmt, "s", $email);
                mysqli_stmt_execute($stmt);
                $result = mysqli_stmt_get_result($stmt);
                

                if (!$row = mysqli_fetch_assoc($result)) {
                    header("Location: ../forgot-password.php?error=sqlerror");
                    exit();
                }
                else {
                    if ($email != $row['emailUsers']) {
                        header("Location: ../forgot-password.php?error=noemail");
                        exit();
                    }
                    else if ($question != $row['questionUsers']) {
                        header("Location: ../forgot-password.php?error=noquestion");
                        exit();
                    }
                    else if (!password_verify($answer,$row['answerUsers'])) {
                        header("Location: ../forgot-password.php?error=noanswer");
                        exit();
                    }
                    else if ($email == $row['emailUsers'] && $question == $row['questionUsers'] && password_verify($answer,$row['answerUsers'])) {

                        $sql = "SELECT * FROM histPassword WHERE histEmail=?";
                        $stmt = mysqli_stmt_init($conn);

                        if (!mysqli_stmt_prepare($stmt,$sql)) {
                            header("Location: ../forgot-password.php?error=sqlerror");
                            exit();
                        }
                        else {

                            $newpassHash = password_hash($newpass, PASSWORD_DEFAULT);
                            mysqli_stmt_bind_param($stmt, "s",$email);
                            mysqli_stmt_execute($stmt);
                            // mysqli_stmt_store_result($stmt);
                            // $resultCheck = mysqli_stmt_num_rows($stmt);
                            $result = mysqli_stmt_get_result($stmt);
                            $matchflag = 0;

                            while ($row = mysqli_fetch_assoc($result)) {

                                if ($email == $row['histEmail'] && password_verify($newpass,$row['histPass'])) {
                                    $matchflag = 1;
                                    break;
                                }

                            }

                            if ($matchflag == 1) {
                                header("Location: ../forgot-password.php?error=usedpass");
                            }
                            else if ($matchflag == 0) {

                                $sql = "UPDATE users SET pwdUsers=? WHERE emailUsers=?";
                                $stmt = mysqli_stmt_init($conn);

                                if (!mysqli_stmt_prepare($stmt,$sql)) {
                                    header("Location: ../forgot-password.php?error=sqlerror");
                                    exit();
                                }
                                else {

                                    $newpassHash = password_hash($newpass, PASSWORD_DEFAULT);
                                    mysqli_stmt_bind_param($stmt, "ss",$newpassHash,$email);
                                    mysqli_stmt_execute($stmt);

                                    $sql = "INSERT INTO histPassword (histEmail, histPass) VALUES (?,?)";
                                    $stmt = mysqli_stmt_init($conn);

                                    if (!mysqli_stmt_prepare($stmt,$sql)) {
                                        header("Location: ../forgot-password.php?error=sqlerror");
                                        exit();
                                    }
                                    else {

                                        mysqli_stmt_bind_param($stmt,"ss",$email,$newpassHash);
                                        mysqli_stmt_execute($stmt);

                                        header("Location: ../forgot-password.php?reset=success");

                                    }

                                }

                            }

                        }

                    }
                    else {
                        header("Location: ../forgot-password.php?error=nouser");
                        exit();
                    }

                }

            }

        }

        mysqli_stmt_close($stmt);
        mysqli_close($conn);

    }
    else {
        header("Location: ../forgot-password.php");
        exit();
    }

?>