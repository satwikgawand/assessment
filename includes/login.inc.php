<?php

if (isset($_POST['login-submit'])) {

    require 'dbh.inc.php';

    $idmail = $_POST['idmail'];
    $password = $_POST['pwd'];

    if (empty($idmail) || empty($password)) {
        header("Location: ../login.php?error=emptyfields");
        exit();
    }
    else {
        
        $sql = "SELECT * FROM users WHERE usernameUsers=? OR emailUsers=?";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location: ../login.php?sqlerror");
            exit();
        }
        else {

            mysqli_stmt_bind_param($stmt, "ss", $idmail, $idmail);
            mysqli_stmt_execute($stmt);

            $result = mysqli_stmt_get_result($stmt);

            if ($row = mysqli_fetch_assoc($result)) {
                
                $passwordCheck = password_verify($password, $row['pwdUsers']);

                if ($passwordCheck == false) {
                    header("Location: ../login.php?error=wrongpassword");
                    exit();
                }
                else if ($passwordCheck == true) {
                    
                    session_start();
                    // $_SESSION['userId'] = $row['idUsers'];
                    $_SESSION['userUsername'] = $row['usernameUsers'];

                    // header("Location: ../index.php?login=success");
                    header("Location: ../test.php");
                    // exit();

                }
                else {
                    header("Location: ../login.php?error=wrongpassword");
                    exit();
                }

            }
            else {
                header("Location: ../login.php?error=nouser");
                exit();
            }

        }

    }

    mysqli_stmt_close($stmt);
    mysqli_close($conn);

}
else {
    header("Location: ../login.php");
    exit();
}

?>