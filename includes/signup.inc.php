<?php

if (isset($_POST['signup-submit'])) {

    require 'dbh.inc.php';

    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['pwd'];
    $passwordRepeat = $_POST['pwd-repeat'];
    $securityQuestion = $_POST['security-question'];
    $securityAnswer = $_POST['security-answer'];

    if (empty($username) || empty($email) || empty($password) || empty($passwordRepeat) || empty($securityQuestion) || empty($securityAnswer)) {
        header("Location: ../signup.php?error=emptyfields&username=".$username."&email=".$email);
        exit();
    }
    else if (!filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        header("Location ../signup.php?error=invaliduseremail");
    }
    else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        header("Location: ../signup.php?error=invalidusername&username=".$username);
        exit();
    }
    else if (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        header("Location: ../signup.php?error=invalidemail&email=".$email);
        exit();
    }
    else if ($securityQuestion == "select") {
        header("Location: ../signup.php?error=invalidsecurityquestion");
        exit();
    }
    else if ($password !== $passwordRepeat) {
        header("Location: ../signup.php?error=passwordcheck&username=".$username."&email=".$email);
        exit();
    }
    else {

        $sql = "SELECT usernameUsers FROM users WHERE usernameUsers=?";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("Location ../signup.php?error=sqlerror");
            exit();
        }
        else {
            
            mysqli_stmt_bind_param($stmt, "s", $username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);

            if ($resultCheck > 0) {
                header("Location: ../signup.php?error=usertaken&email=".$email);
                exit();
            }
            else {

                $sql = "SELECT emailUsers FROM users WHERE emailUsers=?";
                $stmt = mysqli_stmt_init($conn);

                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    header("Location ../signup.php?error=sqlerror");
                    exit();
                }
                else {

                    mysqli_stmt_bind_param($stmt, "s", $email);
                    mysqli_stmt_execute($stmt);
                    mysqli_stmt_store_result($stmt);
                    $resultCheck = mysqli_stmt_num_rows($stmt);

                    if ($resultCheck > 0) {
                        header("Location: ../signup.php?error=emailtaken&username=".$username);
                        exit();
                    }
                    else {

                        $sql = "INSERT INTO users (usernameUsers, emailUsers, pwdUsers, questionUsers, answerUsers) VALUES (?, ?, ?, ?, ?)";
                        $stmt = mysqli_stmt_init($conn);

                        if (!mysqli_stmt_prepare($stmt, $sql)) {
                            header("Location ../signup.php?error=sqlerror");
                            exit();
                        }
                        else {

                            $passwordHash = password_hash($password, PASSWORD_DEFAULT);
                            $answerHash = password_hash($securityAnswer, PASSWORD_DEFAULT);
                            mysqli_stmt_bind_param($stmt, "sssss", $username, $email, $passwordHash, $securityQuestion, $answerHash);
                            mysqli_stmt_execute($stmt);

                            $sql = "INSERT INTO histPassword (histEmail, histPass) VALUES(?,?)";
                            $stmt = mysqli_stmt_init($conn);

                            if (!mysqli_stmt_prepare($stmt, $sql)) {
                                header("Location ../signup.php?error=sqlerror");
                                exit();
                            }
                            else {

                                mysqli_stmt_bind_param($stmt, "ss", $email, $passwordHash);
                                mysqli_stmt_execute($stmt);

                                header("Location: ../signup.php?signup=success");

                            }

                        }

                    }

                }

            }
            
        }

    }

    mysqli_stmt_close($stmt);
    mysqli_close($conn);

}
else {
    header("Location: ../signup.php");
}

?>