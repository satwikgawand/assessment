<?php

session_start();

error_reporting(0);

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="description" content="JurisTech Assessment for User Authentication with PHP and MySQL.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <script src="form-validation.js"></script>
</head>

<body>
    <div class="wrapper">
        <h1 class="title">JurisTech Assessment</h1>
        <div class="container">
            <h1 class="form-title">Signup</h1>

            <?php

                if (isset($_GET['error'])) {

                    if ($_GET['error'] == "emptyfields") {
                        echo '<p class="text-error">Fill in all fields!</p>';
                    }
                    else if ($_GET['error'] == "invaliduseremail") {
                        echo '<p class="text-error">Invalid username and email!</p>';
                    }
                    else if ($_GET['error'] == "invalidusername") {
                        echo '<p class="text-error">Invalid username!</p>';
                    }
                    else if ($_GET['error'] == "invalidemail") {
                        echo '<p class="text-error">Invalid email!</p>';
                    }
                    else if ($_GET['error'] == "passwordcheck") {
                        echo '<p class="text-error">Passwords do not match!</p>';
                    }
                    else if ($_GET['error'] == "usertaken") {
                        echo '<p class="text-error">The username is already taken! Please choose another.</p>';
                    }
                    else if ($_GET['error'] == "emailtaken") {
                        echo '<p class="text-error">The email is already taken! Please login via the login page <a class="toggle" href="login.php">here</a></p>';
                    }
                    else if ($_GET['error'] == "invalidsecurityquestion") {
                        echo '<p class="text-error">Please select a security question</p>';
                    }

                }
                else if ($_GET['signup'] == "success") {
                    echo '<p class="text-success">Signup successful! Please <a class="toggle" href="login.php">Login</a> to continue</p>';
                }

            ?>

            <form id="signup-form" class="login-form" action="includes/signup.inc.php" method="post">

                <input onkeyup="usernameValidation()" id="username" class="form-field" type="text" name="username" placeholder="username"><br />
                <span id="username-helper" class="text-error helper">Username must contain more than 5 characters.</span><br />
                <input onkeyup="emailValidation()" id="email" class="form-field" type="text" name="email" placeholder="email"><br />
                <span id="email-helper" class="text-error helper">Please enter a valid email addres.</span><br />
                <input onkeyup="passwordValidation()" id="password" class="form-field" type="password" name="pwd" placeholder="password"><br />
                <span id="password-helper" class="text-error helper">Password must be at least 8 characters long.</span><br />
                <input onkeyup="repeatPasswordValidation()" id="repeat-password" class="form-field" type="password" name="pwd-repeat" placeholder="repeat password"><br />
                <span id="repeat-password-helper" class="text-error helper">Passwords do not match.</span><br />

                <select id="security-question" class="form-field" name="security-question">
                    <option value="select">- - select security question - -</option>
                    <option value="first-kiss">What is the first name of the person you first kissed?</option>
                    <option value="first-fail">What is the last name of the teacher who gave you your first failing grade?</option>
                    <option value="favorite-childhood-friend">What is the name of your favorite childhood friend?</option>
                    <option value="first-job-place">In what city or town was your first job?</option>
                    <option value="mother-father-meet">In what city or town did your mother and father meet?</option>
                </select><br />

                <input id="security-answer" class="form-field" type="text" name="security-answer" placeholder="security answer"><br />
                <button class="form-submit" type="submit" name="signup-submit">Signup</button>

            </form>
            <div class="login-signup">
                <p class="text">have an account? <a class="toggle" href="login.php">Login</a>.</p>
                <p class="text">or go back to the <a class="toggle" href="index.php">main page</a>.</p>
            </div>
        </div>
    </div>
</body>

</html>