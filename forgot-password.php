<?php

session_start();

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="description" content="JurisTech Assessment for User Authentication with PHP and MySQL.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css">
    <script src="form-validation.js"></script>
</head>

<body>
    <div class="wrapper">
        <h1 class="title">JurisTech Assessment</h1>
        <div class="container">
            <h1 class="form-title">New Password.</h1>
            <p class="text">Please answer the security question you chose while signing up.</p>
            <?php
            
                if (isset($_GET['security'])) {

                    if ($_GET['security'] == 'first-kiss') {
                        echo '<p class="text">What is the first name of the person you first kissed?</p>';
                    }
                    else if ($_GET['security'] == 'first-fail') {
                        echo '<p class="text">What is the last name of the teacher who gave you your first failing grade?</p>';
                    }
                    else if ($_GET['security'] == 'favorite-childhood-friend') {
                        echo '<p class="text">What is the name of your favorite childhood friend?</p>';
                    }
                    else if ($_GET['security'] == 'first-job-place') {
                        echo '<p class="text">In what city or town was your first job?</p>';
                    }
                    else if ($_GET['security'] == 'mother-father-meet') {
                        echo '<p class="text">In what city or town did your mother and father meet?</p>';                        
                    }

                }

            ?>

            <?php

                if (isset($_GET['reset'])) {

                    if ($_GET['reset'] == "success") {

                        echo '
                            <p class="text-success">Your password has been reset. Please go to the Login screen to continue.</p>
                            <p class="text-success"><a class="toggle" href="login.php">Click here</a> to redirect to the login page</p>
                        ';

                    }

                }
                else {

                    if (isset($_GET['error'])) {

                        if ($_GET['error'] == "emptyfields") {
                            echo '<p class="text-error">Fill in all the fields!</p>';
                        }
                        else if ($_GET['error'] == "noques") {
                            echo '<p class="text-error">Please select a security question!</p>';
                        }
                        else if ($_GET['error'] == "mismatchpass") {
                            echo '<p class="text-error">Passwords do not match!</p>';
                        }
                        else if ($_GET['error'] == "sqlerror") {
                            echo '<p class="text-error">SQL error!</p>';
                        }
                        else if ($_GET['error'] == "noemail") {
                            echo '<p class="text-error">No account registered with this email! You can Signup <a class="toggle" href="signup.php">here</a>.</p>';
                        }
                        else if ($_GET['error'] == "noquestion" || $_GET['error'] == "noanswer") {
                            echo '<p class="text-error">Security question and/or answer do not match!</p>';
                        }
                        else if ($_GET['error'] == "usedpass") {
                            echo '<p class="text-error">Cannot choose previously used password! Please choose a new unique password.</p>';
                        }
                        else if ($_GET['error'] == "nouser") {
                            echo '<p class="text-error">No account registered with this email! You can Signup <a class="toggle" href="signup.php">here</a>.</p>';
                        }

                    }

                    echo '
                        <form class="login-form" action="includes/forgot-password.inc.php" method="post">
                            <select id="security-question" class="form-field" name="security-question">
                                <option value="select">- - select security question - -</option>
                                <option value="first-kiss">What is the first name of the person you first kissed?</option>
                                <option value="first-fail">What is the last name of the teacher who gave you your first failing grade?</option>
                                <option value="favorite-childhood-friend">What is the name of your favorite childhood friend?</option>
                                <option value="first-job-place">In what city or town was your first job?</option>
                                <option value="mother-father-meet">In what city or town did your mother and father meet?</option>               
                            </select><br />
                            <input class="form-field" type="text" name="security-answer" placeholder="answer for your security question"><br />
                            <input onkeyup="emailValidation()" id="email" class="form-field" type="text" name="forgot-email" placeholder="your email">
                            <span id="email-helper" class="text-error helper">Please enter a valid email addres.</span><br />
                            <input onkeyup="passwordValidation()" id="password" class="form-field" type="password" name="new-pass" placeholder="new password"><br />
                            <span id="password-helper" class="text-error helper">Password must be at least 8 characters long.</span><br />                                        
                            <input onkeyup="repeatPasswordValidation()" id="repeat-password" class="form-field" type="password" name="repeat-new-pass" placeholder="confirm password"><br />
                            <span id="repeat-password-helper" class="text-error helper">Passwords do not match.</span><br />
                            <button class="form-submit" type="submit" name="forgot-submit">Submit</button>
                        </form>
                    ';

                }

            ?>

            <div class="login-signup">
                <p class="text">don't have an account with us yet? <a class="toggle" href="signup.php">Signup</a>.</p>
                <p class="text">or go back to the <a class="toggle" href="index.php">main page</a>.</p>
            </div>
        </div>
    </div>
</body>

</html>